package com.ams.nwayaa.scenes.authenticationScenes.loginScene

import com.ams.nwayaa.common.models.GenericResponse
import com.ams.nwayaa.common.models.LoginResponse
import com.ams.nwayaa.networking.apis.AuthenticationAPIs
import io.reactivex.Single

class LoginRepo (private val apis: AuthenticationAPIs) {
    fun login(userMail: String, password: String): Single<GenericResponse<LoginResponse>> {
       return apis.login(userMail, password)
    }
}