package com.ams.nwayaa.scenes.authenticationScenes.loginScene

import com.ams.nwayaa.R
import com.ams.nwayaa.common.Navigator
import com.ams.nwayaa.common.baseClasses.AppActivity
import com.ams.nwayaa.scenes.authenticationScenes.loginScene.LoginViewStates.SuccessfulLogin
import com.ams.nwayaa.utils.extensions.bindTo
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppActivity<LoginViewModel, LoginViewStates>(LoginViewModel::class) {

    override fun getViewId() = R.layout.activity_login

    override fun initUI() {
        emailTextInputEditText.hint = ""
        passwordTextInputEditText.hint = ""
        emailTextInputEditText.setOnFocusChangeListener { _, hasFocus ->
            emailTextInputLayout.error = null
            if (!hasFocus) emailTextInputEditText.hint = ""
            else emailTextInputEditText.hint = resources.getString(R.string.email_example_hint)
        }
        passwordTextInputEditText.setOnFocusChangeListener { _, hasFocus ->
            passwordTextInputLayout.error = null
            if (!hasFocus) passwordTextInputEditText.hint = ""
            else passwordTextInputEditText.hint = resources.getString(R.string.password_example_hint)
        }
        // Buttons click actions
        loginButton.setOnClickListener { getViewModel().login() }
        forgetPasswordButton.setOnClickListener { Navigator.openForgetPasswordActivity() }
        createNewAccountButton.setOnClickListener { Navigator.openCreateAccountStepOneActivity() }
    }

    override fun bindViews() {
        emailTextInputEditText.bindTo { getViewModel().email = it }
        passwordTextInputEditText.bindTo { getViewModel().password = it }
    }

    override fun onViewStateChanged(state: LoginViewStates) {
        when(state) {
            is SuccessfulLogin -> {
                hideLoading()
                Navigator.openHomeActivity()
            }
            is LoginViewStates.EmailError -> {
                emailTextInputEditText.hint = ""
                emailTextInputLayout.error = resources.getString(R.string.invalid_email)
            }
            is LoginViewStates.PasswordError -> {
                passwordTextInputEditText.hint = ""
                passwordTextInputLayout.error = resources.getString(R.string.invalid_password)
            }
            is LoginViewStates.Error -> {
                hideLoading()
                showError(state.errorMessage)
            }
            is LoginViewStates.ShowLoading -> { showLoading() }
            is LoginViewStates.HideLoading -> { hideLoading() }
        }
    }
}

