package com.ams.nwayaa.scenes.authenticationScenes.loginScene

import com.ams.nwayaa.common.baseClasses.AppViewModel
import com.ams.nwayaa.utils.extensions.addTo
import com.ams.nwayaa.utils.extensions.applyLoadingBehaviour

class LoginViewModel(private val loginRepo: LoginRepo): AppViewModel<LoginViewStates>() {

    var email: String = ""
    var password: String = ""

    fun login() {
        if (email.isEmpty()) postViewState(LoginViewStates.EmailError)
        if(password.isEmpty()) postViewState(LoginViewStates.PasswordError)
        else {
            postViewState(LoginViewStates.ShowLoading)
            loginRepo.login(email, password)
                    .applyLoadingBehaviour()
                    .subscribe({ response ->
                        // response.data?.accessToken
                        response.data?.user
                        postViewState(LoginViewStates.SuccessfulLogin())
                    }, { throwable  ->
                        postViewState(LoginViewStates.Error(throwable.toString()))
                    })
                    .addTo(compositeDisposable)
        }
    }
}