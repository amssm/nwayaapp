package com.ams.nwayaa.scenes.authenticationScenes.loginScene

sealed class LoginViewStates {
    class SuccessfulLogin(): LoginViewStates()
    class Error(val errorMessage: String): LoginViewStates()
    object EmailError: LoginViewStates()
    object PasswordError: LoginViewStates()
    object ShowLoading: LoginViewStates()
    object HideLoading: LoginViewStates()
}