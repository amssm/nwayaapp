package com.ams.nwayaa.common.baseClasses.networking

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AppRestEndpoints {

    @GET("competitions/PL/matches")
    // @Headers("X-Auth-Token:" + BuildConfig.Auth_Token)
    fun getProjects(@Query("dateFrom") dateFrom: String, @Query("dateTo") dateTo: String): Single<String>
}