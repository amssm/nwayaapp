package com.ams.nwayaa.common.injection

import com.ams.nwayaa.scenes.authenticationScenes.loginScene.LoginViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val viewModelModules: Module = module {
   viewModel { LoginViewModel(get()) }
}