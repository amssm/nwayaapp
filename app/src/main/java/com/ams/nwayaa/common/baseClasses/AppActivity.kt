package com.ams.nwayaa.common.baseClasses

import com.ams.androiddevkit.baseClasses.designPatterns.mvvm.BaseMVVMActivity
import com.ams.nwayaa.R
import com.ams.nwayaa.utils.ViewUtils
import kotlin.reflect.KClass

abstract class AppActivity<ViewModel: AppViewModel<States>, States>(klazz: KClass<ViewModel>): BaseMVVMActivity<ViewModel, States>(klazz) {
    fun showLoading() { ViewUtils.dialogs.showLoading(this, getString(R.string.loading)) }
    fun hideLoading() { ViewUtils.dialogs.hideLoading() }
    fun showError(message: String) { ViewUtils.dialogs.showDialog(this, getString(R.string.error), message) }
}