package com.ams.nwayaa.common

import com.ams.androiddevkit.baseClasses.BaseNavigator
import com.ams.androiddevkit.utils.extensions.open
import com.ams.nwayaa.common.application.NawayaApp
import com.ams.nwayaa.scenes.authenticationScenes.loginScene.LoginActivity

object Navigator: BaseNavigator() {

    override fun getActivityContext() = NawayaApp.instance.getCurrentActivity()!!

    fun openCreateAccountStepOneActivity() {

    }

    fun openCreateAccountStepTwoActivity() {

    }

    fun openLoginActivity() {
        getActivityContext().open<LoginActivity>()
    }

    fun openForgetPasswordActivity() {
        getActivityContext().open<LoginActivity>()
    }

    fun openHomeActivity() {

    }
}
