package com.ams.nwayaa.common.baseClasses.networking

import com.ams.androiddevkit.baseClasses.networking.BaseRestClient
import com.ams.androiddevkit.baseClasses.networking.GsonUtils
import com.ams.androiddevkit.utils.extensions.getConverterFactory
import com.ams.nwayaa.BuildConfig

class AppRestClient(): BaseRestClient() {

    override fun getBaseURL() = BuildConfig.BASE_URL

    override fun getConverterFactory() = GsonUtils().getCustomGsonConverter().getConverterFactory()

    override fun isDebuggable() = BuildConfig.DEBUG
}
