package com.ams.nwayaa.common.application

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.ams.nwayaa.BuildConfig
import com.ams.nwayaa.common.injection.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.module.Module
import androidx.multidex.MultiDex
import com.ams.androiddevkit.baseClasses.BaseApplication

class NawayaApp: BaseApplication() {

    companion object { lateinit var instance: NawayaApp }

    private val activityLifecycleCallbacks = ActivityLifecycleCallbacks()

    override fun getAppContext() = this@NawayaApp

    override fun getCurrentActivity() = activityLifecycleCallbacks.currentActivity as AppCompatActivity?

    override fun onCreate() {
        super.onCreate()
        instance = this
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun injectKoin() {
        startKoin {
            logger(getKoinLoggingLevel())
            androidContext(getAppContext())
            modules(getKoinModules())
        }
    }

    override fun getKoinLoggingLevel() = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()

    override fun getKoinModules(): List<Module> {
        return listOf(
                viewModelModules,
                repositoryModules,
                restClientModule,
                restAPIsModules,
                otherModules)
    }
}