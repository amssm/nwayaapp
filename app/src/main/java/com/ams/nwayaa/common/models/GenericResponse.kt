package com.ams.nwayaa.common.models

data class GenericResponse<T>(val message: String, val data: T? = null)