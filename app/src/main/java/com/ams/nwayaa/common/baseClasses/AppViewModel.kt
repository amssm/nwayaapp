package com.ams.nwayaa.common.baseClasses

import androidx.lifecycle.LifecycleOwner
import com.ams.androiddevkit.baseClasses.designPatterns.mvvm.BaseViewModel
import io.reactivex.disposables.CompositeDisposable

open class AppViewModel<States>: BaseViewModel<States>() {

    protected var compositeDisposable = CompositeDisposable()

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        compositeDisposable.dispose()
    }
}