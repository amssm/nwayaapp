package com.ams.nwayaa.common.models

data class LoginResponse(val accessToken: String, val user: User? = null, val expires_in: Long)