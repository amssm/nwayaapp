package com.ams.nwayaa.common.injection

import com.ams.androiddevkit.baseClasses.networking.IRetrofitClient
import com.ams.nwayaa.networking.apis.AuthenticationAPIs
import com.ams.nwayaa.common.baseClasses.networking.AppRestClient
import org.koin.core.module.Module
import org.koin.dsl.module

val restAPIsModules: Module = module {
    factory { get<IRetrofitClient>().getRetrofitClient(AuthenticationAPIs::class.java) }
}

val restClientModule: Module = module {
    single { AppRestClient() as IRetrofitClient }
}