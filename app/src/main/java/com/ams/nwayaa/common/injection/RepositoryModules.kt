package com.ams.nwayaa.common.injection

import com.ams.nwayaa.scenes.authenticationScenes.loginScene.LoginRepo
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModules: Module = module {
    factory { LoginRepo(get()) }
}