package com.ams.nwayaa.networking.apis

import com.ams.nwayaa.common.models.GenericResponse
import com.ams.nwayaa.common.models.LoginResponse
import com.ams.nwayaa.networking.Endpoints
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthenticationAPIs {
    @POST(Endpoints.LOGIN)
    fun login(@Query("email") userMail: String,
              @Query("password") password: String): Single<GenericResponse<LoginResponse>>
    @POST(Endpoints.LOGOUT)
    fun logout(): Completable
}