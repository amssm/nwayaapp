package com.ams.nwayaa.networking

object Endpoints {
    const val LOGIN = "login"
    const val LOGOUT = "logout"
}