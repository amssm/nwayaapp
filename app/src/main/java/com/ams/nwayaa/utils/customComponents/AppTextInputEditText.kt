package com.ams.nwayaa.utils.customComponents

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.res.ResourcesCompat
import com.ams.nwayaa.R
import com.google.android.material.textfield.TextInputEditText

open class AppTextInputEditText(context: Context, attributeSet: AttributeSet?): TextInputEditText(context, attributeSet) {

    init {
        initTextInputEditTextFont(context)
    }

    constructor(context: Context): this(context, null) {}

    @Suppress("MemberVisibilityCanBePrivate")
    protected fun initTextInputEditTextFont(context: Context) {
        val typeface = ResourcesCompat.getFont(context, R.font.noto_kufi_arabic_regular)
        setTypeface(typeface)
    }
}