@file:Suppress("unused")

package com.ams.nwayaa.utils.extensions

import android.widget.EditText
import com.ams.nwayaa.utils.customComponents.AppTextInputEditText
import com.jakewharton.rxbinding.widget.RxTextView

fun AppTextInputEditText.bindTo(setValue:(String) -> Unit) {
    RxTextView
            .textChanges(this)
            .subscribe { charSequence -> setValue(charSequence.toString()) }
}

fun EditText.bindTo(setValue:(String) -> Unit) {
    RxTextView
            .textChanges(this)
            .subscribe{ charSequence -> setValue(charSequence.toString()) }
}